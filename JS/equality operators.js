// Strict Equality (Type + Value)
console.log(1 === 1);  //true
console.log('1' === 1); //false


// Llose Equality (Value)
console.log(1 == 1); // true
console.log('1' == 1); //true  because it reats it as '1' == '1'
console.log(true == 1); //true  because it reats it as true == true

