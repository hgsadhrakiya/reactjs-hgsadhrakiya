// promise

let promiseToCleanTheRoom = new Promise(function(resolve, reject) {

  //cleaning the room

  let isClean = false;

  if (isClean) {
    resolve('Clean');
  } else {
    reject('not Clean');
  }

});

promiseToCleanTheRoom.then(function(fromResolve) {
  console.log('the room is' + fromResolve);
}).catch(function(fromReject){
	console.log('the room is' + fromReject);
})
// promise ends


// Filter

let numbers = [1, -1, 52, 3];

let positives = numbers.filter(function(value) {
	return value >= 0;
});
// Filter Ends

// Arrow function
let numbers = [1, -1, 52, 3];

let positives = numbers.filter(value =>
	value >= 0
);
// Arrow function ends

// Reduce
let numbers = [1, -1, 52, 3];

let sumN = numbers.reduce(function(total, value) {
	return total + value;
}, 0); // 0 is initialization of total

let sumN = numbers.reduce((total, value) => {
	return total + value;
}, 0); // 0 is initialization of total

// Reduce ends

// Map
var numbers = [4, 9, 16, 25];

let sqrtNums = numbers.map(Math.sqrt);

// map method calls the function on each elements of the array and generates new array having the returned things.

// The map() method calls the provided function once for each element in an array, in order.

// Map Ends


Async
---------
ajax
promise
callback
timer



Types of function
------------------

1. Function declaration
// function declaration
function isEven(num) {
  return num % 2 === 0;
}
isEven(24); // => true
isEven(11); // => false




JavaScript does support multithreading via Web Workers.



// headers in jquery

$.ajax({
     url: "http://localhost/PlatformPortal/Buyers/Account/SignIn",
     data: { signature: authHeader },
     type: "GET",
     beforeSend: function(xhr){xhr.setRequestHeader('X-Test-Header', 'test-value');},
     success: function() { alert('Success!' + authHeader); }
  });

$.ajax({
    url: "/test",
    headers: {"X-Test-Header": "test-value"}
});

// headers in jquery ends


// sequirity in jquery ajax call
That's why you need authentication and authorization handling in your server side code. 

Authentication is typically handled by a username and password; it is the act of verifying a user is who he is.

Authorization can be handled by Roles on the server, and is the check to make sure the user can do what they are trying to do.
// sequirity in jquery ajax call end



CSS Quesions
--------------
em - elements (i.e., relative to the font-size of the element; e.g., 2 em means 2 times the current font size)


console.log('a');
setTimeout(function() {

console.log('b');
}, 10);
console.log('c');

a
c
b


